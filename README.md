# Practical exam

This repository has the purpose of giving a "mission" to the Funx company's candidates. It evaluates technical capabilities, problem solving, git management and agility.

## Minimum objectives

* Integrate a list of matches.
* Save individual matches in the phone calendar.

You're free to develop other features. 

## Layout

We recommend this layout, but you can use any other you see fit. Feel free to make the best.

![Alt text](layout.png)

## API

```
http://futbol.masfanatico.cl/api/u-chile/match/in_competition/transicion2017
```

## IOS Rules

* Swift 3 or 4
* You may use cocoapods

## Android Rules

* Java or Kotlin
* You may use gradle

## Submitting

After you're done developing, make a pull request to this repository. It will be reviewed and then rejected.








